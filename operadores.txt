Tipos de Operadores

Operadores Aritméticos:
	Suma + .
	Resta - .
	Multiplicación * .
	División / .
	Resto de la División % .

Operadores de Asignación:
	'+=' : op1 += op2 à op1 = op1 + op2
	'-=' : op1 -= op2 à op1 = op1 - op2
	'*=' : op1 *= op2 à op1 = op1 * op2
	'/=' : op1 /= op2 à op1 = op1 / op2
	'%=' : op1 %= op2 à op1 = op1 % op2

Operadores Unarios:
	+ y - Para cambiar el signo del operando. 

Operador Instanceof: Nos permite saber si un objeto pertenece a una clase o no.
NombreObjeto instanceof NombreClase

Operadores Incrementales:
	'++'
	'--'

Operadores Relacionales:
	'>':	Mayor que
	'<':	Menor que
	'==':	Iguales
	'!=':	Distintos
	'>=': Mayor o igual que
	'<=':	Menor o igual que

Operadores Lógicos:
	'&&' :	devuelve true si ambos operandos son true.
	'||' : devuelve true si alguno de los operandos son true.
	'!' : Niega el operando que se le pasa.
	'&' : devuelve true si ambos operandos son true, evaluándolos ambos.
	'|' : devuelve true uno de los operandos es true, evaluándolos ambos.

Operador de concatenación con cadena de caracteres '+':
Por Ejemplo: System.out.println("El total es"+ result +"unidades");
