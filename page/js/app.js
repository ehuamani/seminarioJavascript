/**
 * Validación de formulario en javascript puro
 * 
 * */
function validateForm(){
		var nombre = document.forms["form_contact"]["txtnombre"].value;
		var email = document.forms["form_contact"]["txtemail"].value;
		var telefono = document.forms["form_contact"]["txttelefono"].value;
		var mensaje = document.forms["form_contact"]["txtmensaje"].value;
		if(nombre == null || nombre == ""){
			alert("Ingrese Nombre");
			return false;
		}
		if(email == null ||  email == ""){
			alert("Ingrese Email");
			return false;
		}
		if(telefono == null || telefono == ""){
			alert("Ingrese Telefono");
			return false;
		}
		if(mensaje == null || mensaje == ""){
			alert("Ingrese Mensaje");
			return false;
		}
		return true;
}



$(document).ready(function(){
	$("#form_contact").submit(function(event){
		event.preventDefault();
		
		var nombre = $("#form_contact #txtnombre").val();
		var email = $("#form_contact #txtemail").val();
		var telefono = $("#form_contact #txttelefono").val();
		var mensaje = $("#form_contact #txtmensaje").val();
		
		var validate = true;
		
		if(nombre == null || nombre == ""){
			alert("Ingrese Nombre");
			validate = false;
		}
		if(email == null ||  email == ""){
			alert("Ingrese Email");
			validate = false;
		}
		if(telefono == null || telefono == ""){
			alert("Ingrese Telefono");
			validate = false;
		}
		if(mensaje == null || mensaje == ""){
			alert("Ingrese Mensaje");
			validate = false;
		}
		
		if(validate == true){
			$("#form_contact").submit();
		}
		
		});
});
