/**
Definición de variables
*/
var numero1 = 20;
var numero2 = 5;
var nombre = "Edgar";
var peso = 55.5;
var resultado = nombre + (numero1 / numero2);
/**
Función simple sin parámetros
*/
function crearMensaje(){
    var mensaje1 = "Mensaje 1";
    alert(mensaje1);
}
/**
Función con 1 parámetro
*/
function calcularEdad(panionacimiento){
    var edad = 2015-panionacimiento;
    alert("Tu edad es: "+edad);
}
/**
Solicitamos ingreso de dato
*/
var edad = prompt("Ingrese Año de Nacimiento: ");
/**
Llamar una función que recibe un parámetro
*/
calcularEdad(edad);
